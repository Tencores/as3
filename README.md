
## Quick start
###MY HOSTING URL: https://prog8110-2a11b.firebaseapp.com/
**********************************************************************

### 1. Download this repository
```
git clone https://AllenYangCAD@bitbucket.org/Tencores/as3.git
```

Repository will be downloaded into `my-app/` folder

### 2. Instal dependencies

Go to the downloaded repository folder and run:
```
npm install
```

### 3. Run the app

```
PORT=1234 npm start
```

App will be opened in browser at `http://localhost:1234/`

### 4. Host on Firebase

```
npm run login

npm run use

npm run deploy
```
